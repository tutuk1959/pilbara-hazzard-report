$(document).ready(function(){
	$(".hazzardclosed_yes").hide();
	$(".hazzardclosed_no").hide();
	
	$('.datepicker').daterangepicker({
		locale: {
			format: 'DD-MM-YYYY'
		},
		singleDatePicker: true,
	});
	
	$('.department').select2({
		placeholder: 'Type department name',
		ajax: {
			url: '/load_department',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
				results:$.map(data, function (item) {
					return {
						text: item.department,
						id: item.iddepartment
					}
				})
				};
			}
		}
	});
	
	$('.location').select2({
		placeholder: 'Type location name or postcode',
		ajax: {
			url: '/load_location',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
				results:$.map(data, function (item) {
					return {
						text: item.location+" - "+item.postcode,
						id: item.idlocation
					}
				})
				};
			}
		}
	});
	
	$('.supervisor').select2({
		placeholder: 'Type supervisor name',
		ajax: {
			url: '/load_supervisor',
			dataType: 'json',
			delay: 250,
			processResults: function (data) {
				return {
				results:$.map(data, function (item) {
					return {
						text: item.name,
						id: item.idemployee
					}
				})
				};
			}
		}
	});
	
	$('#hazardclosed_yes').on("click", function(){
		$(".hazzardclosed_yes").slideDown("slow");
		$(".hazzardclosed_no").hide();
	}); 
	
	$('#hazardclosed_no').on("click", function(){
		$(".hazzardclosed_no").slideDown("slow");
		$(".hazzardclosed_yes").hide();
	});
	
	
	$('#supervisor_report_list').DataTable( {
	});
});