<?php

return [
	'company'     			=> 'Pilbara',
	'full_address'			=> '7 / 48 Prindiville Drive',
	'zipcode'                => '6065',
	'phone'					=> '0896 5859 5043',
	'email'              	 => 'support@somsweb.com.au',
	'abn'             		 => 'XX - XXXX - XXXX',
];
