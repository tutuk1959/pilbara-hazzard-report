@extends('layouts.template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Staff Submission</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
									<li class="breadcrumb-item active">Staff Submission</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@elseif (session('status') == 'mail_danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Hazzard Report, Staff Submission for: </strong> {{$employee->name}}
								</div>
									
								<div class="card-body">
									<form action="/staff_submission" method="POST">
										{{csrf_field()}}
										<input type="hidden" name="idemployee" value="{{$employee->idemployee}}"/>
										<div class="form-row">
										
											<div class="col-12 col-md-6">
												<label for="reported_by">Reported By</label>
												<input id="reported_by" type="text" class="form-control" name="reported_by" value="{{$employee->name}}" readonly required="true" />
											</div>
											<div class="col-12 col-md-6">
												<label for="department">Department</label>
												<select class="department form-control" name="department" id="department" >
													
												</select>
												
											</div>
										</div>
										<div class="form-row">
											<div class="col-12 col-md-6">
												<label for="date">Date</label>
												<input id="date" type="text" class="form-control datepicker" name="date" value="{{old('date')}}" required="true"/>
											</div>
											<div class="col-12 col-md-3">
												<label for="time">Time</label>
												<input id="time" type="text" class="form-control" name="time" value="{{old('time')}}"/>
											</div>
											<div class="col-12 col-md-3">
												<label for="am_pm">AM / PM</label>
												<select class="form-control select2am_pm" name="am_pm" id="am_pm">
													<option value="1">AM</option>
													<option value="2">PM</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="location">Location</label>
											<select class="location form-control" name="location" id="location" >
												
											</select>
										</div>
										<div class="form-group">
											<label for="hazard">Hazzard</label>
											<textarea name="hazard" id="hazard" class="form-control">{{old('hazard')}}</textarea>
										</div>
										
										<div class="form-group">
											<label for="action">Action Taken</label>
											<textarea name="action" id="action" class="form-control">{{old('action')}}</textarea>
										</div>
										
										<p>Case closed? </p>
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" 
											name="hazardclosed" id="hazardclosed_yes" <?=(old('action_closed') == 1) ? 'checked' : '' ?> value="1">
											<label class="form-check-label" for="hazardclosed_yes">Yes</label>
										</div>
										
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="hazardclosed" 
											id="hazardclosed_no" <?=(old('action_closed') == 2) ? 'checked' : '' ?> value="2">
											<label class="form-check-label" for="hazardclosed_no">No</label>
										</div>
										
										<div class="form-group hazzardclosed_yes">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="handtosupervisor" id="handtosupervisor" <?=(old('handtosupervisor') == 1) ? 'checked' : '' ?> value="1">
												<label class="form-check-label" for="handtosupervisor">Hand to Supervisor</label>
											</div>
										
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="finished" 
												<?=(old('finished') == 1) ? 'checked' : '' ?>
												id="finished" value="1">
												<label class="form-check-label" for="finished">Finished</label>
											</div>
										</div>
										
										<div class="form-group hazzardclosed_no">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="disscusswithsupervisor" id="disscusswithsupervisor" <?=(old('disscusswithsupervisor') == 1) ? 'checked' : '' ?> value="1">
												<label class="form-check-label" for="disscusswithsupervisor">Discuss with Supervisor</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="supervisortocomplete" id="supervisortocomplete" <?=(old('supervisortocomplete') == 1) ? 'checked' : '' ?> value="1">
												<label class="form-check-label" for="supervisortocomplete">Supervisor completion</label>
											</div>
										</div>
										
										<div class="form-group hazzardclosed_no">
											<label  class="form-label"  for="supervisor">Supervisor</label>
											<select class="supervisor form-control" name="supervisor" id="supervisor" >
												
											</select>
										</div>
										
										<div class="form-group">
											<button type="submit" class="btn btn-primary" value="Submit">Save</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
