@extends('layouts.template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Supervisor Submission</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
									<li class="breadcrumb-item active">Supervisor Submission</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@elseif (session('status') == 'mail_danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Hazzard Reports List, Staff Submission for: </strong> {{$employee->name}} 
								</div>
									
								<div class="card-body">
									<form action="/staff_report" method="POST">
										{{csrf_field()}}
										@if(!is_null($supervisor))
											<input type="hidden" name="idsupervisor" value="{{$supervisor->idemployee}}"/>
										@endif
										
										<input type="hidden" name="idhazardreport" value="{{$hazardreport->idhazardreport}}"/>
										<input type="hidden" name="idemployee" value="{{$hazardreport->idemployee}}"/>
										<div class="form-row">
											<div class="col-12 col-md-6">
												<label for="reported_by">Reported By</label>
												<input id="reported_by" type="text" class="form-control" name="reported_by" value="{{$employee->name}}" readonly  />
											</div>
											<div class="col-12 col-md-6">
												<label for="department">Department</label>
												<select class="department form-control" name="department" id="department" >
													
												</select>
											</div>
										</div>
										<div class="form-row">
											<div class="col-12 col-md-6">
												<label for="date">Date</label>
												<?php $date = date('d-m-Y', strtotime($hazardreport->date));?>
												<input id="date" type="text" class="form-control datepicker" name="date" value="{{$date}}"  required="true"/>
											</div>
											<div class="col-12 col-md-3">
												<label for="time">Time</label>
												<input id="time" type="text" class="form-control" name="time" value="{{$hazardreport->time}}"  />
											</div>
											<div class="col-12 col-md-3">
												<label for="am_pm">AM / PM</label>
												<select class="form-control select2am_pm" name="am_pm" id="am_pm" >
													<option value="1" <?=($hazardreport->ampm == 1) ? 'selected' : '' ?>>AM</option>
													<option value="2" <?=($hazardreport->ampm == 2) ? 'selected' : '' ?> >PM</option>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label for="location">Location</label>
											<select class="location form-control" name="location" id="location" >
												
											</select>
										</div>
										<div class="form-group">
											<label for="hazard">Hazzard</label>
											<textarea name="hazard" id="hazard" class="form-control" >{{$hazardreport->hazard}}</textarea>
										</div>
										
										<div class="form-group">
											<label for="action">Action Taken</label>
											<textarea name="action" id="action" class="form-control" >{{$hazardreport->actiontaken}}</textarea>
										</div>
										
										<p>Case closed? </p>
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="hazardclosed" id="hazardclosed_yes" <?=($hazardreport->hazardclosed == 1) ? 'checked' : '' ?>  value="1" >
											<label class="form-check-label" for="hazardclosed_yes" >Yes</label>
										</div>
										
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="hazardclosed" id="hazardclosed_no" <?=($hazardreport->hazardclosed == 2) ? 'checked' : '' ?> value="2" >
											<label class="form-check-label" for="hazardclosed_no">No</label>
										</div>
										
										<div class="form-group hazzardclosed_yes">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="handtosupervisor" id="handtosupervisor" <?=($hazardreport->hazardclosed_yes == 1) ? 'checked' : '' ?> value="1" >
												<label class="form-check-label" for="handtosupervisor">Hand to Supervisor</label>
											</div>
										
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="finished" id="finished" <?=($hazardreport->hazardclosed_yes_2 == 1) ? 'checked' : '' ?>  value="1"  >
												<label class="form-check-label" for="finished">Finished</label>
											</div>
										</div>
										
										<div class="form-group hazzardclosed_no">
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="disscusswithsupervisor" id="disscusswithsupervisor" value="1" <?=($hazardreport->hazardclosed_no == 1) ? 'checked' : '' ?> >
												<label class="form-check-label" for="disscusswithsupervisor">Discuss with Supervisor</label>
											</div>
											<div class="form-check form-check-inline">
												<input class="form-check-input" type="checkbox" name="supervisortocomplete" id="supervisortocomplete" <?=($hazardreport->hazardclosed_no_2 == 1) ? 'checked' : '' ?> value="1" >
												<label class="form-check-label" for="supervisortocomplete">Supervisor completion</label>
											</div>
										</div>
										<div class="form-group hazzardclosed_no">
											<label  class="form-label"  for="supervisor">Supervisor</label>
											<select class="supervisor form-control" name="supervisor" id="supervisor">
												
											</select>
										</div>
										<div class="form-group">
											<button type="submit" class="btn btn-primary" value="Submit">Save</button>
										</div>
									</form>
									<hr />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
