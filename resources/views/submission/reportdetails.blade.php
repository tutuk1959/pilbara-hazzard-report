@extends('layouts.template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Hazard Reports</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
									<li class="breadcrumb-item active">List</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@elseif (session('status') == 'mail_danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Hazzard Report, Supervisor Submission for: </strong> {{$employee->name}}
								</div>
									
								<div class="card-body">
									<div class="button-wrapper mb-3">
										<a href="{{url('/report_pdf')}}/{{$hazardreport->idhazardreport}}" target="_blank" class="btn btn-primary mr-3">Convert to PDF</a>
										<a href="{{url('/send_email')}}/{{$hazardreport->idhazardreport}}" class="btn btn-warning">Send me email copy</a>
									</div>
									
									<div class="table-responsive">
										<table class="table table-bordered">
											<tr>
												<td width="30%"colspan="2" rowspan="2" align="center" style="background-color:#000;color:#fff;">
													<h4>Hazard <br /> Report</h4>
												</td>
												
												<td>
													<strong>Reported by :</strong> {{$hazardreport->name}}
												</td>
												
											</tr>
											<tr>
												<td>
													<strong>Department :</strong> {{$hazardreport->department}}
												</td>
											</tr>
											<tr>
												<td colspan="2" ><strong>Date :</strong> <?php echo date('d-M-Y', strtotime($hazardreport->date));?></td>
												<td><strong>Time :</strong> {{$hazardreport->time}} @if($hazardreport->ampm == 1) {{'AM'}} @elseif($hazardreport->ampm == 2) {{'PM'}} @endif</td>
												
											</tr>
											<tr>
												<td colspan="3"><strong>Location :</strong> {{$hazardreport->location}} - {{$hazardreport->suburb}} , {{$hazardreport->postcode}}</td>
											</tr>
											<tr>
												<td colspan="3"><strong>Hazard :</strong> {{$hazardreport->hazard}}</td>
											</tr>
											<tr>
												<td colspan="3"><strong>Action Taken :</strong> {{$hazardreport->actiontaken}}</td>
											</tr>
											<tr>
												<td colspan="3"><strong>Hazard Closed :</strong> 
													@if($hazardreport->hazardclosed == 1)
														{{'Yes'}}
														- 
														@if(!is_null($hazardreport->hazardclosed_yes))
															<em>Hand to Supervisor</em>
														@endif
														--
														@if(!is_null($hazardreport->hazardclosed_yes_2))
															<em>Finished</em>
														@endif
													@elseif($hazardreport->hazardclosed == 2)
														{{'No'}}
														- 
														@if(!is_null($hazardreport->hazardclosed_no))
															<em>Discuss with Supervisor</em>
														@endif
														--
														@if(!is_null($hazardreport->hazardclosed_no_2))
															<em>Supervisor's completion</em>
														@endif
													@endif
												</td>
											</tr>
											
											@if($hazardreport->hazardclosed == 2 && !is_null($hazardreport->idsupervisor))
												<tr>
													<td colspan="3">
														
													</td>
												</tr>
												<tr>
													<td colspan="3">
														<strong>Supervisor Name: </strong>{{$employee->name}}
													</td>
												</tr>
												@if(!is_null($hazardreport->supervisor_action))
													<tr>
														<td colspan="3">
															<strong>Supervisor Action: </strong>{{$hazardreport->supervisor_action}}
														</td>
													</tr>
												@else
													<tr>
														<td colspan="3">
															<strong>Supervisor Action: </strong><em>No action yet</em>
														</td>
													</tr>
												@endif
												@if(!is_null($hazardreport->entered_bms))
													<tr>
														<td colspan="3">
															<strong>Entered into BMS: </strong>
															@if($hazardreport->entered_bms == 1)
																{{'Yes'}}
															@elseif($hazardreport->entered_bms ==2)
																{{'No'}}
															@endif
														</td>
													</tr>
												@else
													<tr>
														<td colspan="3">
															<strong>Entered to BMS: </strong><em>Has not decided yet</em>
														</td>
													</tr>
												@endif
												@if(!is_null($hazardreport->actions_assigned))
													<tr>
														<td colspan="3">
															<strong>Actions assigned: </strong>
															@if($hazardreport->actions_assigned == 1)
																{{'Yes'}}
															@elseif($hazardreport->actions_assigned ==2)
																{{'No'}}
															@endif
														</td>
													</tr>
												@else
													<tr>
														<td colspan="3">
															<strong>Actions assigned: </strong><em>Has not decided yet</em>
														</td>
													</tr>
												@endif
												@if(!is_null($hazardreport->action_closed))
													<tr>
														<td colspan="3">
															<strong>Actions closed: </strong>
															@if($hazardreport->action_closed == 1)
																{{'Yes'}}
															@elseif($hazardreport->action_closed ==2)
																{{'No'}}
															@endif
														</td>
													</tr>
												@else
													<tr>
														<td colspan="3">
															<strong>Actions closed: </strong><em>Has not decided yet</em>
														</td>
													</tr>
												@endif
												@if(!is_null($hazardreport->supervisor_signed) && !is_null($hazardreport->supervisor_signed_yes))
													<tr>
														<td colspan="3">
															<strong>Supervisor Signature: </strong>
															{{$hazardreport->supervisor_signed}}
														</td>
													</tr>
												@else
													<tr>
														<td colspan="3">
															<strong>Supervisor Signature: </strong><em>No signature</em>
														</td>
													</tr>
												@endif
											@endif
										
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
