@extends('layouts.template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Hazard Reports</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
									<li class="breadcrumb-item active">My Submission</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@elseif (session('status') == 'mail_danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Hazzard Reports List, Staff Submission for: </strong> {{$employee->name}}
								</div>
									
								<div class="card-body">
									<p>This list is showing reports that submitted by {{$employee->name}}.</p>
									<div class="table-responsive">
										<table id="supervisor_report_list" class="table table-bordered table-hover display">
											<thead>
												<tr>
													<th>Supervisor</th>
													<th>Department</th>
													<th>Date</th>
													
													<th>Hazard</th>
													<th>Status</th>
													<th>Action</th>
												</tr>
											</thead>
											<tbody>
												@if(!is_null($hazardreport))
													@foreach($hazardreport as $report)
														<tr>
															<td>
																@if(!is_null($report->supervisor_name))
																	{{$report->supervisor_name}}
																@else
																	<em>{{'Not assigned yet'}}</em>
																@endif
															</td>
															<td>{{$report->department}}</td>
															<td><?php echo date('d-m-Y', strtotime($report->date));?></td>
															<td>{{$report->hazard}}</td>
															<td>
																@if($report->hazardclosed == '1')
																	{{'Resolved'}}
																@elseif($report->hazardclosed == '2')
																	{{'Unresolved'}}
																@endif
															</td>
															<td>
																<a href="{{url('/staff_report/')}}/{{$report->idhazardreport}}" class="btn btn-primary mb-2 mr-2">Submission</a>
																<a href="{{url('report_details/')}}/{{$report->idhazardreport}}" class="btn btn-warning mb-2">View</a>
															</td>
														</tr>
													@endforeach
												@else
													<p>No report data available</p>
												@endif
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
