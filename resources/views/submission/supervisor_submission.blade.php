@extends('layouts.template')
@section('content')
		<div class="content-page">
			<div class="content">
				<div class="container-fluid">
					<div class="row">
						<div class="col-xl-12">
							<div class="breadcrumb-holder">
								<h1 class="main-title float-left">Supervisor Submission</h1>
								<ol class="breadcrumb float-right">
									<li class="breadcrumb-item"><a href="{{url('dashboard')}}">Home</a></li>
									<li class="breadcrumb-item active">Supervisor Submission</li>
								</ol>
								<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@elseif (session('status') == 'mail_danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
					</div>
					<div class="row">
						<div class="col-12">						
							<div class="card mb-3 mt-2">
								<div class="card-header">
									<strong>Hazzard Report {{date('d-m-Y'), strtotime('$hazardreport->date')}} - {{$hazardreport->department}}, Supervisor Submission for: </strong> {{$employee->name}}
								</div>
									
								<div class="card-body">
									<div class="form-row">
										<div class="col-12 col-md-6">
											<label for="reported_by">Reported By</label>
											<input id="reported_by" type="text" class="form-control" name="reported_by" value="{{$employee->name}}" readonly  />
										</div>
										<div class="col-12 col-md-6">
											<label for="department">Department</label>
											<input id="reported_by" type="text" class="form-control" name="department" value="{{$hazardreport->department}}" readonly  />
											
										</div>
									</div>
									<div class="form-row">
										<div class="col-12 col-md-6">
											<label for="date">Date</label>
											<?php $date = date('d-m-Y', strtotime($hazardreport->date));?>
											<input id="date" type="text" class="form-control datepicker" name="date" value="{{$date}}" readonly required="true"/>
										</div>
										<div class="col-12 col-md-3">
											<label for="time">Time</label>
											<input id="time" type="text" class="form-control" name="time" value="{{$hazardreport->time}}" readonly />
										</div>
										<div class="col-12 col-md-3">
											<label for="am_pm">AM / PM</label>
											<select class="form-control select2am_pm" name="am_pm" id="am_pm" readonly>
												<option value="1" <?=($hazardreport->ampm == 1) ? 'selected' : '' ?>>AM</option>
												<option value="2" <?=($hazardreport->ampm == 2) ? 'selected' : '' ?> >PM</option>
											</select>
										</div>
									</div>
									<div class="form-group">
										<label for="location">Location</label>
										<input id="location" type="text" class="form-control" name="location" value="{{$hazardreport->location}}" readonly  />
									</div>
									<div class="form-group">
										<label for="hazard">Hazzard</label>
										<textarea name="hazard" id="hazard" class="form-control" readonly>{{$hazardreport->hazard}}</textarea>
									</div>
									
									<div class="form-group">
										<label for="action">Action Taken</label>
										<textarea name="action" id="action" class="form-control" readonly>{{$hazardreport->actiontaken}}</textarea>
									</div>
									
									<p>Case closed? </p>
									<div class="form-check form-check-inline mb-3">
										<input class="form-check-input" type="radio" name="hazardclosed" id="hazardclosed_yes" <?=($hazardreport->hazardclosed == 1) ? 'checked' : '' ?>  value="1" disabled>
										<label class="form-check-label" for="hazardclosed_yes" >Yes</label>
									</div>
									
									<div class="form-check form-check-inline mb-3">
										<input class="form-check-input" type="radio" name="hazardclosed" id="hazardclosed_no" <?=($hazardreport->hazardclosed == 2) ? 'checked' : '' ?> value="2" disabled>
										<label class="form-check-label" for="hazardclosed_no">No</label>
									</div>
									
									<div class="form-group hazzardclosed_yes">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" name="handtosupervisor" id="handtosupervisor" <?=($hazardreport->hazardclosed_yes == 1) ? 'checked' : '' ?> value="1" disabled>
											<label class="form-check-label" for="handtosupervisor">Hand to Supervisor</label>
										</div>
									
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" name="finished" id="finished" <?=($hazardreport->hazardclosed_yes_2 == 1) ? 'checked' : '' ?>  value="1" disabled >
											<label class="form-check-label" for="finished">Finished</label>
										</div>
									</div>
									
									<div class="form-group hazzardclosed_no">
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" name="disscusswithsupervisor" id="disscusswithsupervisor" value="1" <?=($hazardreport->hazardclosed_no == 1) ? 'checked' : '' ?> disabled >
											<label class="form-check-label" for="disscusswithsupervisor">Discuss with Supervisor</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="checkbox" name="supervisortocomplete" id="supervisortocomplete" <?=($hazardreport->hazardclosed_no_2 == 1) ? 'checked' : '' ?> value="1" disabled>
											<label class="form-check-label" for="supervisortocomplete">Supervisor completion</label>
										</div>
									</div>
									<hr />
									<form action="/supervisor_submission" method="POST">
										{{csrf_field()}}
										<input type="hidden" name="idsupervisor" value="{{$supervisor->idemployee}}"/>
										<input type="hidden" name="idhazardreport" value="{{$hazardreport->idhazardreport}}"/>
										<input type="hidden" name="idemployee" value="{{$hazardreport->idemployee}}"/>
										<div class="form-group">
											<label for="supervisor_name">Supervisor Name</label>
											<input id="supervisor_name" type="text" class="form-control" name="supervisor_name" value="{{$supervisor->name}}" readonly  />
										</div>
										<div class="form-group">
											<label for="supervisoraction">Supervisor Action</label>
											<textarea name="supervisoraction" id="supervisoraction" class="form-control" >@if($errors->has('supervisoraction')){{old('supervisoraction')}}@endif @if(!is_null($hazardreport->supervisor_action)){{$hazardreport->supervisor_action}}@endif</textarea>
										</div>
										<p>Entered into BMS ?</p>
										
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="bms" id="bms" value="1" <?=($errors->has('bms')) ? (old('bms') == 1 ? 'checked' : '') : '' ?>  <?=(!is_null($hazardreport->entered_bms)) ? ($hazardreport->entered_bms == 1 ? 'checked' : '') : '' ?>  >
											<label class="form-check-label" for="bms" >Yes</label>
										</div>
									
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="bms" id="bms" value="2" <?=($errors->has('bms')) ? (old('bms') == 2 ? 'checked' : '') : '' ?>  <?=(!is_null($hazardreport->entered_bms)) ? ($hazardreport->entered_bms == 2 ? 'checked' : '') : '' ?> >
											<label class="form-check-label" for="bms">No</label>
										</div>
										
										<p>Actions Assigned? </p>
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="action_assigned" id="action_assigned"  value="1" <?=($errors->has('action_assigned')) ? (old('action_assigned') == 1 ? 'checked' : '') : '' ?>  <?=(!is_null($hazardreport->actions_assigned)) ? ($hazardreport->actions_assigned == 1 ? 'checked' : '') : '' ?> >
											<label class="form-check-label" for="action_assigned" >Yes</label>
										</div>
										
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="action_assigned" id="action_assigned" value="2" <?=($errors->has('action_assigned')) ? (old('action_assigned') == 2 ? 'checked' : '') : '' ?>  <?=(!is_null($hazardreport->actions_assigned)) ? ($hazardreport->actions_assigned == 2 ? 'checked' : '') : '' ?>  >
											<label class="form-check-label" for="action_assigned">No</label>
										</div>
										
										<p>Actions Closed? </p>
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="action_closed" id="action_closed" value="1" <?=($errors->has('action_closed')) ? (old('action_closed') == 1 ? 'checked' : '') : '' ?>  <?=(!is_null($hazardreport->action_closed)) ? ($hazardreport->action_closed == 1 ? 'checked' : '') : '' ?> >
											<label class="form-check-label" for="action_assigned" >Yes</label>
										</div>
									
										<div class="form-check form-check-inline mb-3">
											<input class="form-check-input" type="radio" name="action_closed" id="action_closed" value="2" <?=($errors->has('action_closed')) ? (old('action_closed') == 2 ? 'checked' : '') : '' ?>  <?=(!is_null($hazardreport->action_closed)) ? ($hazardreport->action_closed == 2 ? 'checked' : '') : '' ?>>
											<label class="form-check-label" for="action_assigned">No</label>
										</div>
										
										<div class="form-group">
											<label for="signature">Supervisor Action</label>
											<textarea name="signature" id="signature" class="form-control" readonly>{{$supervisor->name}}</textarea>
										</div>
										
										<input type="hidden" name="supervisor_signed_yes" value="1"/>
										<div class="form-group">
											<button type="submit" class="btn btn-primary" value="Submit">Save</button>
										</div>
									</form>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
