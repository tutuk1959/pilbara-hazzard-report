<htmlpageheader name="page-header">
	Hazard Form Report
</htmlpageheader>
	<!DOCTYPE html>
	<html>
		<head>
			 <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
			 <style >
				@media print { 
					
					body{
						font-size:11px;
						line-height:1.5px;
						font-family:Arial,sans,serif;
					}
					.table-responsive{
						margin:10px 0;
						line-height:1.3;
					}
					table{
						line-height:1.3;
					}
					
				} 
				
			</style>
		</head>
		<body>
			<div class="table-responsive">
				<table class="table table-condensed" border="0" width="100%" cellpadding="3">
					<tr style="text-align:center">
						<td width="50%">
							
						</td>
						<td width="50%" style="padding-left: 20px;text-align:right">
							<address style="font-style:12px;">
								{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
							</address>
						</td>
					</tr>
					
				</table>
			</div>
			<div class="table-responsive">
				<table class="table table-bordered" border="1" cellpadding="5" cellspacing="3" width="100%">
					<tr>
						<td colspan="2" rowspan="2" align="center" style="background-color:#000;color:#fff;">
							<h1>Hazard Report</h1>
						</td>
						
						<td >
							<strong>Reported by :</strong> {{$employee->name}}
						</td>
						
					</tr>
					<tr>
						<td>
							<strong>Department :</strong> {{$hazardreport->department}}
						</td>
					</tr>
					<tr>
						<td colspan="2" ><strong>Date :</strong> <?php echo date('d-M-Y', strtotime($hazardreport->date));?></td>
						<td><strong>Time :</strong> {{$hazardreport->time}} @if($hazardreport->ampm == 1) {{'AM'}} @elseif($hazardreport->ampm == 2) {{'PM'}} @endif</td>
						
					</tr>
					<tr>
						<td colspan="3"><strong>Location :</strong> <br />{{$hazardreport->location}} - {{$hazardreport->suburb}} , {{$hazardreport->postcode}}</td>
					</tr>
					<tr>
						<td colspan="3"><strong>Hazard :</strong> <br /> {{$hazardreport->hazard}}</td>
					</tr>
					<tr>
						<td colspan="3"><strong>Action Taken :</strong> <br />{{$hazardreport->actiontaken}}</td>
					</tr>
					<tr>
						<td colspan="3"><strong>Hazard Closed :</strong> 
							@if($hazardreport->hazardclosed == 1)
								{{'Yes'}}
								- 
								@if(!is_null($hazardreport->hazardclosed_yes))
									<em>Hand to Supervisor</em>
								@endif
								--
								@if(!is_null($hazardreport->hazardclosed_yes_2))
									<em>Finished</em>
								@endif
							@elseif($hazardreport->hazardclosed == 2)
								{{'No'}}
								- 
								@if(!is_null($hazardreport->hazardclosed_no))
									<em>Discuss with Supervisor</em>
								@endif
								--
								@if(!is_null($hazardreport->hazardclosed_no_2))
									<em>Supervisor's completion</em>
								@endif
							@endif
						</td>
					</tr>
					
					@if($hazardreport->hazardclosed == 2 && !is_null($hazardreport->idsupervisor))
						<tr>
							<td colspan="3">
								
							</td>
						</tr>
						<tr>
							<td colspan="3">
								<strong>Supervisor Name: </strong>{{$supervisor->name}}
							</td>
						</tr>
						@if(!is_null($hazardreport->supervisor_action))
							<tr>
								<td colspan="3">
									<strong>Supervisor Action: </strong> <br /> {{$hazardreport->supervisor_action}}
								</td>
							</tr>
						@else
							<tr>
								<td colspan="3">
									<strong>Supervisor Action: </strong><em> <br /> No action yet</em>
								</td>
							</tr>
						@endif
						@if(!is_null($hazardreport->entered_bms))
							<tr>
								<td colspan="3">
									<strong>Entered into BMS: </strong>
									@if($hazardreport->entered_bms == 1)
										{{'Yes'}}
									@elseif($hazardreport->entered_bms ==2)
										{{'No'}}
									@endif
								</td>
							</tr>
						@else
							<tr>
								<td colspan="3">
									<strong>Entered to BMS: </strong><em>Has not decided yet</em>
								</td>
							</tr>
						@endif
						@if(!is_null($hazardreport->actions_assigned))
							<tr>
								<td colspan="3">
									<strong>Actions assigned: </strong>
									@if($hazardreport->actions_assigned == 1)
										{{'Yes'}}
									@elseif($hazardreport->actions_assigned ==2)
										{{'No'}}
									@endif
								</td>
							</tr>
						@else
							<tr>
								<td colspan="3">
									<strong>Actions assigned: </strong><em>Has not decided yet</em>
								</td>
							</tr>
						@endif
						@if(!is_null($hazardreport->action_closed))
							<tr>
								<td colspan="3">
									<strong>Actions closed: </strong>
									@if($hazardreport->action_closed == 1)
										{{'Yes'}}
									@elseif($hazardreport->action_closed ==2)
										{{'No'}}
									@endif
								</td>
							</tr>
						@else
							<tr>
								<td colspan="3">
									<strong>Actions closed: </strong><em>Has not decided yet</em>
								</td>
							</tr>
						@endif
						@if(!is_null($hazardreport->supervisor_signed) && !is_null($hazardreport->supervisor_signed_yes))
							<tr>
								<td colspan="3">
									<strong>Supervisor Signature: </strong> <br />
									{{$hazardreport->supervisor_signed}}
								</td>
							</tr>
						@else
							<tr>
								<td colspan="3">
									<strong>Supervisor Signature: </strong><em> <br /> No signature</em>
								</td>
							</tr>
						@endif
					@endif
										
				</table>
			</div>
		</body>
	</html>
<htmlpagefooter name="page-footer">
	{PAGENO}
</htmlpagefooter>