<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Pilbara Hazzard Form</title>
		<link rel="shortcut icon" href="public/assets/images/favicon.ico">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/plugins/datetimepicker/css/daterangepicker.css')}}" rel="stylesheet" /> 
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
		<link rel="stylesheet" href="{{url('assets/plugins/select2/css/select2.min.css')}}" />
		<script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>
<body class="adminbody">
	<div id="main">
		<div class="headerbar">
			<div class="headerbar-left">
				<a href="{{url('/')}}" class="logo">
					<img src="{{url('assets/images/images.png')}}" alt="Logo" class="avatar-rounded">
				</a>
			</div>
			<nav class="navbar-custom">
				
				<ul class="list-inline float-right mb-0">
					<li class="list-inline-item dropdown notif">
						<a class="nav-link dropdown-toggle nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
							<img src="{{url('assets/images/admin.png')}}" alt="Profile image" class="avatar-rounded">
						</a>
						<div class="dropdown-menu dropdown-menu-right profile-dropdown ">
							<!-- item-->
							<div class="dropdown-item noti-title">
								<h5 class="text-overflow"><small>{{Auth::user()->email}}</small> </h5>
							</div>

							

							<!-- item-->
							<a href="{{url('/logout')}}" class="dropdown-item notify-item">
								<i class="fa fa-power-off"></i> <span>Logout</span>
							</a>

						</div>
					</li>
				</ul>
				
				<ul class="list-inline menu-left mb-0">
					<li class="float-left">
						<button class="button-menu-mobile open-left">
							<i class="fa fa-fw fa-bars"></i>
						</button>
					</li>
				</ul>
			</nav>
		</div>
		<!-- End Navigation -->
		<!-- Left Sidebar -->
		<div class="left main-sidebar">
			<div class="sidebar-inner leftscroll">
				<div id="sidebar-menu">
					<ul>
						<li class="submenu">
							<a {{{ (Request::is('home') ? 'class=active' : '') }}} {{{ (Request::is('dashboard') ? 'class=active' : '') }}} href="{{url('home')}}"><i class="fa fa-fw fa-info-circle"></i><span> Dashboard</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('staff_submission') ? 'class=active' : '') }}} href="{{url('/staff_submission')}}"><i class="fa fa-fw fa-id-card-o"></i><span> Staff Submission</span> </a>
						</li>
						<li class="submenu">
							<a {{{ (Request::is('supervisor_report_list') ? 'class=active' : '') }}} href="{{url('/supervisor_report_list')}}"><i class="fa fa-fw fa-id-card-o"></i><span> Supervisor Submission</span> </a>
						</li>
						@hasanyrole('one|two|three')
							<li class="submenu">
								<a {{ (Request::is('mysubmission') ? 'class=active' : '') }} href="{{url('/mysubmission')}}"><i class="fa fa-fw fa-file"></i><span>My Submissions</span> </a>
							</li>
						@endhasanyrole
					</ul>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
		<!-- End Sidebar -->
		@section('content')
		@show
		<!-- END content-page -->
		
		<footer class="footer">
			<span class="text-right">
				Copyright <a target="_blank" href="https://somsweb.com.au">SOMS Web</a>
			</span>
		</footer>
	</div>
<!-- END main -->
<script src="{{url('assets/plugins/switchery/switchery.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/moment.min.js')}}"></script>
<script src="{{url('assets/plugins/datetimepicker/js/daterangepicker.js')}}"></script>
<script src="{{url('assets/plugins/select2/js/select2.min.js')}}"></script>
<script src="{{url('assets/js/script.js')}}"></script>
</body>
</html>