<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Pilbara Hazzard Report</title>
		<link rel="shortcut icon" href="public/assets/images/skipbin-logo_03.png">
		<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/font/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css"/>
</head>
<body class="adminbody">
	<div id="main">
		<!-- End Sidebar -->
		@section('login_content')
		@show
	</div>
<!-- END main -->

<script src="{{url('assets/js/bootstrap.min.css')}}{{url('assets/js/modernizr.min.js')}}"></script>
<script src="{{url('assets/js/jquery.min.js')}}"></script>
<script src="{{url('assets/js/moment.min.js')}}"></script>
<script src="{{url('assets/js/popper.min.js')}}"></script>
<script src="{{url('assets/js/bootstrap.min.js')}}"></script>
<script src="{{url('assets/js/detect.js')}}"></script>
<script src="{{url('assets/js/fastclick.js')}}"></script>
<script src="{{url('assets/js/jquery.blockUI.js')}}"></script>
<script src="{{url('assets/js/jquery.nicescroll.js')}}"></script>
<script src="{{url('assets/js/pikeadmin.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
</body>
</html>