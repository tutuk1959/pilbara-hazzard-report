@extends('layouts.template')
@section('content')
	<div class="content-page">
		<div class="content">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xl-12">
						<div class="breadcrumb-holder">
							<h1 class="main-title float-left">Dashboard</h1>
							
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="col-12">
						<div class="card mb-3">
							<div class="card-header">
								<h3><i class="fa fa-bell-o"></i> Welcome, {{$employee->name}} !</h3>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
@endsection
