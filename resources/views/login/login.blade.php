@extends('layouts.login_template')
@section('login_content')
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 text-center mb-3">
							<h1>Hazzard Report</h1>
						</div>
						
						<div class="col-12 col-md-6 offset-md-3">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
							@if (!empty(session('status')))
							<div class="col-12">
								@if (session('status') == 'danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
									</div>
								@elseif (session('status') == 'success')
									<div class="alert alert-success" role="alert">
											{{session('message')}} <br />
										
									</div>
								@elseif (session('status') == 'mail_danger')
									<div class="alert alert-danger" role="alert">
											{{session('message')}} <br />
										
									</div>
								@endif
							</div>
						@endif
						</div>
						<div class="col-12 col-md-6 offset-md-3">						
							<div class="card mb-3 mt-3">
								<div class="card-header">
									<strong>Employee Login</strong>
								</div>
									
								<div class="card-body">
									<form action="/login" method="POST">
										{{csrf_field()}}
										<div class="form-group">
											<label for="email">Email</label>
											<input class="form-control" name="email" id="email" aria-describedby="email" required="" type="email">
										</div>
										<div class="form-group">
											<label for="password">Password</label>
											<input class="form-control" id="password" required="" type="password" name="password">
										</div>
										<div class="form-group">
											<div class="form-check">
												<input class="form-check-input" type="checkbox" name="remember" id="remember">
												<label class="form-check-label" for="remember">
													Remember Me
												</label>
											</div>
										</div>
										<button name="submit" type="submit" class="btn btn-primary">Submit</button>
										<a class="float-right mb-3"  href="{{url('/forgotpassword')}}">Forgot password?</a>
										
										
									</form>
									
									
								</div>
								<div class="card-footer">
									<p>Don't have account? </p>
									<a class="btn btn-danger w-100 mb-3"  href="{{url('/register')}}">Sign Up Here </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
