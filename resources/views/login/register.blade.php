@extends('layouts.login_template')
@section('login_content')
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 text-center mb-3">
							<h1>Registration</h1>
						</div>
						
						<div class="col-12 col-md-6 offset-md-3">
							 @if ($errors->any())
								@foreach ($errors->all() as $error)
									<div class="alert alert-danger">
										{{ $error }}
									</div>
								@endforeach
							@endif
						</div>
						<div class="col-12 col-md-6 offset-md-3">
							@if (!empty(session('status')))
								<div class="col-12 ">
									@if (session('status') == 'danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
										</div>
									@elseif (session('status') == 'success')
										<div class="alert alert-success" role="alert">
												{{session('message')}} <br />
											
										</div>
									@elseif (session('status') == 'mail_danger')
										<div class="alert alert-danger" role="alert">
												{{session('message')}} <br />
											
										</div>
									@endif
								</div>
							@endif
						</div>
						<div class="col-12 col-md-6 offset-md-3">						
							<div class="card mb-3 mt-3">
								<div class="card-header">
									<strong>Employee Registration</strong>
								</div>
									
								<div class="card-body">
									<form action="/register" method="POST">
										{{csrf_field()}}
										<div class="form-group">
											<label for="name">Name</label>
											<input class="form-control" name="name" id="name" aria-describedby="email" required="" value="{{old('name')}}" type="text" placeholder="Input your name here">
										</div>
										<div class="form-group">
											<label for="password">Password</label>
											<input class="form-control" name="password" id="password" aria-describedby="email" required="" type="password" placeholder="Input your password here">
										</div>
										<div class="form-group">
											<label for="confirmpassword">Confirm Password</label>
											<input class="form-control" name="confirmpassword" id="confirmpassword" aria-describedby="email" required="" type="password" value="" placeholder="Retype your password again">
										</div>
										<div class="form-group">
											<label for="phone">Phone Number</label>
											<input class="form-control" name="phone" id="phone" aria-describedby="phone" required="" value="{{old('phone')}}" type="text" placeholder="Input your phone number here">
										</div>
										<div class="form-group">
											<label for="email">Email</label>
											<input class="form-control" name="email" id="email" aria-describedby="phone" required="" value="{{old('email')}}" type="email" placeholder="Input your email address here">
										</div>
										<div class="form-group">
											<label for="address">Address</label>
											<textarea name="address" id="address" cols="30" rows="10" class="form-control" placeholder="Type your address">{{old('address')}}</textarea>
										</div>
										<button name="submit" type="submit" class="btn btn-primary">Submit</button>
										
									</form>
									
									
								</div>
								<div class="card-footer">
									<p>Signed in already?</p>
									<a class="btn btn-danger w-100 mb-3"  href="{{url('/login')}}">Sign me in! </a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
