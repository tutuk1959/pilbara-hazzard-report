@extends('layouts.login_template')
@section('content')
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<?php /**
													<img src="{{url('assets/images/skipbin-logo_03.png')}}" />
													
													**/ ?>
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3 >Welcome to the Hazzard Report System</h3><br />
							<p>Hi {{ $name }},</p>
							<p>Thank you for creating an account with us.</p>
							<p>Here is copy of your registration details: </p>
							<p><strong>Name : </strong> {{$username}}</p>
							<p><strong>Email : </strong> {{$password}}</p>
							<p><strong>Phone Number : </strong> {{$phoneNumber}}</p>
							<p><strong>Address : </strong> {{$address}}</p>
							<p>Thank you ! <br /></p>
							<p>Good Day!</p>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
