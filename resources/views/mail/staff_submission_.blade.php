<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link href="{{url('assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{url('assets/font-awesome/css/font-awesome.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{url('assets/font/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />
	<link href="{{url('assets/css/style.css')}}" rel="stylesheet" type="text/css" />
</head>
<body class="adminbody">
	<div id="main">
		 <style >
			body{
				font-size:11px;
				line-height:1.9px;
			}
			.table-responsive{
				margin:10px 0;
				line-height:1.7;
			}
			table{
				line-height:1.7;
			}
		</style>
		<div class="content">
			<div class="container">
				<div class="content-wrapper">
					<div class="row">
						<div class="col-12 mb-3">
							<div class="row align-items-center">
								<div class="col-12">
									<div class="table-responsive">
										<table class="table table-condensed" border="0" width="100%">
											<tr >
												<td width="50%" style="text-align:left">
													<img src="{{url('assets/images/images.png')}}" alt="Logo" class="avatar-rounded"> Hazzard Form
												</td>
												<td width="50%" style="padding-left: 20px;text-align:right">
													<address style="font-style:12px;">
														{{Config::get('invoices.company')}}<br/>
								{{Config::get('invoices.full_address')}}<br/>
								{{Config::get('invoices.zipcode')}} <br/>
								{{Config::get('invoices.phone')}}<br/>
								{{Config::get('invoices.email')}}<br/>
								ABN : {{Config::get('invoices.abn')}}
													</address>
												</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="col-12">
							<h3>You have submitted a new report</h3><br />
							<p>Hi {{ $employee->name }},</p>
							<p>There is a report you need to check. Report summary listed as below:</p>
							<p>Hazard : <strong>{{$hazardreport->hazard}}</strong></p>
							<p>Action taken : <strong>{{$hazardreport->actiontaken}}</strong></p>
							<p>Report status : <strong>
								@if($hazardreport->hazardclosed == 1) 
									{{'Closed'}} 
								@elseif($hazardreport->hazardclosed == 2) 
									{{'Need attention'}}
								@endif
							</strong></p>
							<p>Thank you. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

