<?php

namespace App\Http\Controllers\submission;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\employee;
use App\User;
use App\department;
use App\location;
use App\hazardreport;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Mail;
use DB;
use Validator;
use Redirect;
use PDF;

class submission extends Controller
{
	private $fromemail;
	private $toemail;
	private $pdf_url;
	public function __construct(){
		$this->fromemail = 'admin@ihubcrm.com';
		$this->toemail = 'support@somsweb.com.au';
		$this->pdf_url = 'invoices/pdf/pdf_1.pdf';
	}
	
	/**
		Menu staff submission
	**/
	
	public function index(){
		$user = Auth::user();
		$employee = $this->get_employee_details($user);
		return view('submission.staff', ['employee' => $employee]);
	}
	
	/**
		Menu supervisor submission
	**/
	
	public function supervisor_index(){
		$user = Auth::user();
		
		$report_list = $this->hazardreport_bysupervisor_id($user);
		$employee = $this->get_employee_details($user);
		return view('submission.supervisor', ['hazardreport' => $report_list,'employee' => $employee]);
	}
	
	/**
		Supervisor submission form
	**/
	
	public function supervisor(Request $request){
		$hazardreport = $this->reportfulldetails_id($request['idhazardreport']);
		$supervisor = $this->get_employee_details_byid($hazardreport->idsupervisor);
		$employee =  $this->get_employee_details_byid($hazardreport->idemployee);
		return view('submission.supervisor_submission', ['hazardreport' => $hazardreport,'employee' => $employee, 'supervisor' => $supervisor]);
	}
	
	/**
		Report details for supervisor
	**/
	
	public function report_details(Request $request){
		$user = Auth::user();
		$employee = $this->get_employee_details($user);
		$hazardreport = $this->reportfulldetails_id($request['idhazardreport']);
		return view('submission.reportdetails', ['hazardreport' => $hazardreport,'employee' => $employee]);
	}
	
	/**
		Report details for logged in users
	**/
	
	public function mysubmission(){
		$user = Auth::user();
		$employee = $this->get_employee_details($user);
		$hazardreport = $this->mysubmission_fetch($user);
		return view('submission.staff_report', ['hazardreport' => $hazardreport,'employee' => $employee]);
	}
	
	
	/**
		Staff report edit form
	**/
	
	public function staffreport(Request $request){
		$hazardreport = $this->reportfulldetails_id($request['idhazardreport']);
		$supervisor = $this->get_employee_details_byid($hazardreport->idsupervisor);
		$employee =  $this->get_employee_details_byid($hazardreport->idemployee);
		return view('submission.staff_report_form', ['hazardreport' => $hazardreport,'employee' => $employee, 'supervisor' => $supervisor]);
	}
	
	/**
		Show hazard report for supervisor
	**/
	public function hazardreport_bysupervisor_id($user){
		$hazardreport = $data = DB::table('hazardreport')
			->leftJoin('employee', 'hazardreport.idemployee', '=', 'employee.idemployee')
			->leftJoin('department', 'hazardreport.iddepartment', '=', 'department.iddepartment')
			->leftJoin('location', 'hazardreport.idlocation', '=', 'location.idlocation')
			->where([
				'hazardreport.idsupervisor' =>$user->idemployee, 
				'hazardreport.hazardclosed' => 2
			])
			->get();
		return $hazardreport;
	}
	
	/**
		Show hazard report for logged user
	**/
	public function mysubmission_fetch($user){
		$hazardreport = $data = DB::table('hazardreport')
			->leftJoin('employee', 'hazardreport.idemployee', '=', 'employee.idemployee')
			->leftJoin('department', 'hazardreport.iddepartment', '=', 'department.iddepartment')
			->leftJoin('location', 'hazardreport.idlocation', '=', 'location.idlocation')
			->leftJoin('employee AS supervisor', 'hazardreport.idsupervisor', '=', 'employee.idemployee')
			->select('employee.*','supervisor.name AS supervisor_name', 'department.*', 'location.*', 'department.*', 'hazardreport.*')
			->where([
				'hazardreport.idemployee' =>$user->idemployee, 
			])
			->distinct('hazardreport.idhazardreport')
			->get();
		return $hazardreport;
	}
	
	/**
		Show hazard report not full details
	**/
	public function hazardreport_details($idhazardreport){
		$hazardreport = DB::table('hazardreport')
		->where('idhazardreport', $idhazardreport)
		->first();
		return $hazardreport;
	}
	
	/**
		Show hazard report full details
	**/
	public function reportfulldetails($idemployee,$idhazardreport){
		$hazardreport = DB::table('hazardreport')
		->leftJoin('employee', 'hazardreport.idemployee', '=', 'employee.idemployee')
		->leftJoin('department', 'hazardreport.iddepartment', '=', 'department.iddepartment')
		->leftJoin('location', 'hazardreport.idlocation', '=', 'location.idlocation')
		->where([
			'hazardreport.idhazardreport' =>$idhazardreport ,
			'hazardreport.idsupervisor' =>$idemployee, 
		])
		->first();
		return $hazardreport;
	}
	
	/**
		Show hazard report full details by Id
	**/
	private function reportfulldetails_id($idhazardreport){
		$hazardreport = DB::table('hazardreport')
		->leftJoin('employee', 'hazardreport.idemployee', '=', 'employee.idemployee')
		->leftJoin('department', 'hazardreport.iddepartment', '=', 'department.iddepartment')
		->leftJoin('location', 'hazardreport.idlocation', '=', 'location.idlocation')
		->where([
			'hazardreport.idhazardreport' =>$idhazardreport ,
		])
		->first();
		return $hazardreport;
	}
	
	private function get_employee_details($user){
		$employee = employee::where('idemployee', $user->idemployee)->first();
		return $employee;
	}
	
	private function get_employee_details_byid($id){
		$employee = employee::where('idemployee', $id)->first();
		return $employee;
	}
	
	private function get_supervisor_details($id){
		$supervisor = employee::where([
			'idemployee' => $id, 
			'idposition' => 1
		])->first();
		return $supervisor;
	}
	
	/** 
		PDF Exporter Stream 
	**/
	public function exportpdf_stream(Request $request){
		$hazardreport = $this->reportfulldetails_id($request['idhazardreport']);
		$supervisor = $this->get_employee_details_byid($hazardreport->idsupervisor);
		$employee =  $this->get_employee_details_byid($hazardreport->idemployee);
		$data =  ['hazardreport' => $hazardreport, 'supervisor' => $supervisor, 'employee' => $employee];
		$pdf = PDF::loadView('pdf.report', $data);
		$filename = 'Report '.$hazardreport->name.' - '.date('d-m-Y', strtotime($hazardreport->name)).'.pdf';
		return $pdf->stream($filename);
	}
	
	/** 
		PDF Exporter Save 
	**/
	public function exportpdf_save($idhazardreport){
		$hazardreport = $this->reportfulldetails_id($idhazardreport);
		$supervisor = $this->get_employee_details_byid($hazardreport->idsupervisor);
		$employee =  $this->get_employee_details_byid($hazardreport->idemployee);
		$data =  ['hazardreport' => $hazardreport, 'supervisor' => $supervisor, 'employee' => $employee];
		$pdf = PDF::loadView('pdf.report', $data);
		$filename = 'Report '.$hazardreport->name.' - '.date('d-m-Y', strtotime($hazardreport->name)).'.pdf';
		$url = 'report/pdf/pdf_1.pdf';
		$dest = 'report/pdf/email/'.$filename;
		copy($url, $dest);
		$pdf->save($dest);
		return $dest;
	}
	
	/** 
		Send email with invoice
	**/
	public function send_email_report(Request $request){
		$hazardreport = $this->reportfulldetails_id($request['idhazardreport']);
		$supervisor = $this->get_employee_details_byid($hazardreport->idsupervisor);
		$employee =  $this->get_employee_details_byid($hazardreport->idemployee);
		$attachment = $this->exportpdf_save($request['idhazardreport']);
		try{
			\Mail::to($supervisor->email)->send(new \App\Mail\SendEmailReport($employee, $hazardreport, $supervisor, $attachment));
			\Mail::to($employee->email)->send(new \App\Mail\SendEmailReport($employee, $hazardreport,$supervisor, $attachment));
			$request->session()->flash('status', 'success');
			$request->session()->flash('message', sprintf('Email sent gracefully'));
			return redirect()->route('supervisor_report_list');
		} catch(Exception $ex){
			$request->session()->flash('status', 'danger');
			$request->session()->flash('message', sprintf('Can not send email message. Error %s', $ex));
			return redirect()->route('supervisor_report_list');
		}
	}
	
	/**
		Post staff form
	**/
	public function staffsubmission(Request $request){
		$validate = Validator::make($request->all(), [
			'reported_by' => 'required',
			'department' => 'required',
			'date' => 'required',
			'time' => 'required|max:5',
			'location' => 'required',
			'action' => 'required',
			'hazard' => 'required',
			'hazardclosed' =>'required'
		]);
		
		if($validate->fails()){
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$hazardreport = new hazardreport();
			$hazardreport->idemployee = $request['idemployee'];
			$hazardreport->idsupervisor = $request['supervisor'];
			$hazardreport->idlocation = $request['location'];
			$hazardreport->iddepartment = $request['department'];
			$hazardreport->date = date('Y-m-d', strtotime($request['date']));
			$hazardreport->time = $request['time'];
			$hazardreport->ampm = $request['am_pm'];
			$hazardreport->hazard = $request['hazard'];
			$hazardreport->actiontaken = $request['action'];
			$hazardreport->hazardclosed = $request['hazardclosed'];
			$hazardreport->hazardclosed_yes = $request['handtosupervisor'];
			$hazardreport->hazardclosed_yes_2 = $request['finished'];
			$hazardreport->hazardclosed_no = $request['disscusswithsupervisor'];
			$hazardreport->hazardclosed_no_2 = $request['supervisortocomplete'];
			if($hazardreport->save()){
				$lastid = $hazardreport->id;
				if($hazardreport->hazardclosed == 1){
					
					try{
						$employeeid = $hazardreport->idemployee;
						$hazardreport = $this->hazardreport_details($lastid);
						
						$employee = $this->get_employee_details_byid($employeeid);
						\Mail::to($employee->email)->send(new \App\Mail\StaffSubmission($employee, $hazardreport));
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', sprintf('Email sent gracefully'));
						return redirect()->route('staff_submission');
					} catch(Exception $ex){
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', sprintf('Can not send email message. Error %s', $ex));
						return redirect()->route('staff_submission');
					}
				} elseif($hazardreport->hazardclosed == 2){
					try{
						
						$employeeid = $hazardreport->idemployee;
						$hazardreport = $this->hazardreport_details($lastid);
						$employee = $this->get_employee_details_byid($employeeid);
						$supervisor = $this->get_supervisor_details($hazardreport->idsupervisor);
						\Mail::to($employee->email)->send(new \App\Mail\StaffSubmission($employee, $hazardreport));
						\Mail::to($supervisor->email)->send(new \App\Mail\StaffSubmissionSupervisor($employee, $hazardreport, $supervisor));
						$request->session()->flash('status', 'success');
						$request->session()->flash('message', sprintf('Email sent gracefully'));
						return redirect()->route('staff_submission');
					} catch(Exception $ex){
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', sprintf('Can not send email message. Error %s', $ex));
						return redirect()->route('staff_submission');
					}
				} 
				
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', sprintf('Insert hazzard report failed'));
				return redirect()->route('staff_submission');
			}
		}
	}
	
	/** 
		Supervisor submission
	**/
	public function supervisorsubmission(Request $request){
		$validate = Validator::make($request->all(), [
			'supervisor_name' => 'required',
			'supervisoraction' => 'required',
			'bms' => 'required',
			'action_assigned' => 'required',
			'action_closed' => 'required',
			'signature' => 'required',
			'supervisor_signed_yes' => 'required'
		]);
		
		if($validate->fails()){
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$hazardreport = new hazardreport();
			try{
				$hazardreport->where([
				'idhazardreport' => $request['idhazardreport'],
				'idsupervisor' => $request['idsupervisor']
				])
				->update([
					'supervisor_action' => $request['supervisoraction'], 
					'entered_bms' => $request['bms'],
					'actions_assigned' => $request['action_assigned'],
					'action_closed' => $request['action_closed'],
					'supervisor_signed' => $request['signature'],
					'supervisor_signed_yes' => $request['supervisor_signed_yes']
				]);
				$hazardreport = $this->reportfulldetails_id($request['idhazardreport']);
				$supervisor = $this->get_employee_details_byid($request['idsupervisor']);
				$employee =  $this->get_employee_details_byid($request['idemployee']);
				$attachment = $this->exportpdf_save($request['idhazardreport']);
				try{
					\Mail::to($supervisor->email)->send(new \App\Mail\SendEmailReport($employee, $hazardreport, $supervisor, $attachment));
					\Mail::to($employee->email)->send(new \App\Mail\SendEmailReport($employee, $hazardreport,$supervisor, $attachment));
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', sprintf('Email sent gracefully'));
					return redirect()->route('supervisor_submission', ['idhazardreport' => $request['idhazardreport']]);
				} catch(Exception $ex){
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', sprintf('Can not send email message. Error %s', $ex));
					return redirect()->route('supervisor_submission', ['idhazardreport' => $request['idhazardreport']]);
				}
			}catch(Exception $ex){
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', sprintf('Failed on supervisor submission. Error %s', $ex));
				return redirect()->route('supervisor_submission', ['idhazardreport' => $request['idhazardreport']]);
			}
			
			
		}
	}
	
	/**
		Update staff form
	**/
	public function staffreportpost(Request $request){
		$validate = Validator::make($request->all(), [
			'reported_by' => 'required',
			'department' => 'required',
			'date' => 'required',
			'time' => 'required|max:5',
			'location' => 'required',
			'action' => 'required',
			'hazard' => 'required',
			'hazardclosed' =>'required'
		]);
		
		if($validate->fails()){
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$hazardreport = new hazardreport();
			if($request['hazardclosed'] == 1){
				try{
					$hazardreport->where([
						'idhazardreport' => $request['idhazardreport'],
						'idemployee' => $request['idemployee']
					])
					->update([
						'idlocation' => $request['location'], 
						'iddepartment' => $request['department'],
						'date' => date('Y-m-d', strtotime($request['date'])),
						'time' => $request['time'],
						'ampm' => $request['am_pm'],
						'hazard' => $request['hazard'],
						'actiontaken' => $request['hazard'],
						'hazardclosed' =>  $request['hazardclosed'],
						'hazardclosed_yes' => $request['handtosupervisor'],
						'hazardclosed_yes_2' => $request['finished'],
						'hazardclosed_no' => null,
						'hazardclosed_no_2' =>  null
					]);
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', sprintf('Report updated successfully'));
					return redirect()->route('staff_report', ['idhazardreport' => $request['idhazardreport']]);
				
				}catch(Exception $ex){
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', sprintf('Failed on staff submission. Error %s', $ex));
					return redirect()->route('staff_report', ['idhazardreport' => $request['idhazardreport']]);
				}
			} elseif($request['hazardclosed'] == 2){
				try{
					$hazardreport->where([
						'idhazardreport' => $request['idhazardreport'],
						'idemployee' => $request['idemployee']
					])
					->update([
						'idlocation' => $request['location'], 
						'iddepartment' => $request['department'],
						'date' => date('Y-m-d', strtotime($request['date'])),
						'time' => $request['time'],
						'ampm' => $request['am_pm'],
						'hazard' => $request['hazard'],
						'actiontaken' => $request['hazard'],
						'hazardclosed' =>  $request['hazardclosed'],
						'hazardclosed_yes' => null,
						'hazardclosed_yes_2' => null,
						'hazardclosed_no' => $request['disscusswithsupervisor'],
						'hazardclosed_no_2' =>  $request['supervisortocomplete']
					]);
					$request->session()->flash('status', 'success');
					$request->session()->flash('message', sprintf('Report updated successfully'));
					return redirect()->route('staff_report', ['idhazardreport' => $request['idhazardreport']]);
				
				}catch(Exception $ex){
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', sprintf('Failed on staff submission. Error %s', $ex));
					return redirect()->route('staff_report', ['idhazardreport' => $request['idhazardreport']]);
				}
			}
			
		}
	}
	
	/** 
		AJAX stuffs 
	**/
	public function loadDepartment(Request $request){
		if ($request->has('q')) {
			$params = $request->q;
			$data = DB::table('department')->select('iddepartment', 'department')->where('department', 'LIKE', '%'.$params.'%')->get();
			return response()->json($data);
		}
	}
	
	public function loadLocation(Request $request){
		if ($request->has('q')) {
			$params = $request->q;
			$data = DB::table('location')
			->select('idlocation', 'location', 'postcode')
			->where('location', 'LIKE', '%'.$params.'%')
			->orWhere('postcode', 'LIKE', '%'.$params.'%')
			->get();
			return response()->json($data);
		}
	}
	
	public function loadSupervisor(Request $request){
		if ($request->has('q')) {
			$params = $request->q;
			$data = DB::table('employee')
			->select('idemployee', 'name')
			->where('name', 'LIKE', '%'.$params.'%')
			->where('idposition', 1)
			->get();
			return response()->json($data);
		}
	}
	
	public function loadReport(Request $request){
		$supervisor = Auth::user();
		if ($request->has('q')) {
			$params = $request->q;
			$data = DB::table('hazardreport')
			->leftJoin('employee', 'hazardreport.idemployee', '=', 'employee.idemployee')
			->select('employee.name', 'hazardreport.idhazardreport', 'hazardreport.idemployee', 'hazardreport.date', 
			'hazardreport.hazard', 'hazardreport.actiontaken', 'hazardreport.hazardclosed')
			->where([
				'hazardreport.idsupervisor' =>$supervisor->id, 
				'hazardreport.hazardclosed' => 2
			])
			->get();
			return response()->json($data);
		}
	}
	
}
