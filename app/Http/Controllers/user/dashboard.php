<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\employee;
use App\User;
use Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class dashboard extends Controller
{
	private $user;
	public function __construct()	{
		$this->user = null;
		$this->middleware(['role:one|two|three']);
	}

	private function get_employee_details($user){
		$employee = employee::where('idemployee', $user->idemployee)->first();
		return $employee;
	}
	
	public function index(){
		$user = Auth::user();
		$employee = $this->get_employee_details($user);
		return view('user.dashboard', ['employee' => $employee]);
	}
	
	
}
