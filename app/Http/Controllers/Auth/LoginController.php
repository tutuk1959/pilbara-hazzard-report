<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Validator;
use Redirect;
use App\User;
use Illuminate\Support\Facades\DB;
use Auth;

class LoginController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/
	use AuthenticatesUsers;

	/**
	* Where to redirect users after login.
	*
	* @var string
	*/
	protected $redirectTo = RouteServiceProvider::HOME;

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('guest')->except('logout');
	}
	
	public function index(){
		return view('login.login');
	}
	
	public function login(Request $request){
		$email = $request['email'];
		$password = $request['password'];
		$remember = $request['remember'];
		$rules = array(
			'email' => 'required|email', 
			'password' => 'required|alphaNum'
		);
		
		$validator = Validator::make($request->all() , $rules);
		if ($validator->fails()){
			return Redirect::to('login')->withErrors($validator)->withInput(); 
		} else {
			if($user = Auth::attempt(['email' => $email, 'password' => $password, 'active' => 1], $remember)){
				return redirect()->route('dashboard');
			} else {
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'Invalid Credentials');
				return redirect()->route('login');
			}
		}
	}
	
	public function logout(Request $request){
		Auth::logout();
		$request->session()->flash('status', 'success');
		$request->session()->flash('message', 'Logged out');
		return redirect()->route('login');
	}
}
