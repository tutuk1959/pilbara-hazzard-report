<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Providers\RouteServiceProvider;
use App\User;
use App\employee;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
	/*
	|--------------------------------------------------------------------------
	| Register Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles the registration of new users as well as their
	| validation and creation. By default this controller uses a trait to
	| provide this functionality without requiring any additional code.
	|
	*/

	use RegistersUsers;

	/**
	* Where to redirect users after registration.
	*
	* @var string
	*/
	protected $redirectTo = RouteServiceProvider::HOME;

	/**
	* Create a new controller instance.
	*
	* @return void
	*/
	public function __construct()
	{
		$this->middleware('guest');
	}
	
	
	public function index(){
		return view('login.register');
	}
	
	public function register(Request $request){
		$name = $request['name'];
		$phonenumber = $request['phone'];
		$email = $request['email'];
		$address = $request['address'];
		$password = $request['password'];
		$confirmpassword = $request['confirmpassword'];
		
		$validate = Validator::make($request->all(), [
			'email' => 'required|email',
			'password' => 'required',
			'confirmpassword' => 'required|same:password',
		],[
			'email.email' => 'Please type a valid email address', 
		]);
		
		if($validate->fails()){
			return Redirect::back()->withErrors($validate)->withInput($request->all());
		} else {
			$employee = new employee;
			$check = $employee->where(
				'email',$email)
			->first();
			
			if($check){
				$request->session()->flash('status', 'danger');
				$request->session()->flash('message', 'User with same email exists!');
				return redirect()->route('register')->withInput($request->all());
			} else {
				try{
					$employee->name = $request['name'];
					$employee->phone = $request['phone'];
					$employee->email = $request['email'];
					$employee->address = $request['address'];
					$employee->idposition = 2;
					
					$employee->save();
					$last_employee_id = $employee->id;
					$user = new User;
					try{
						$user->email = $request['email'];
						$user->password = Hash::make($request['password']);
						$user->email_verified = 0;
						$user->idemployee = $last_employee_id;
						$user->role = 3;
						$user->active = 1;
						$user->assignRole('three');
						$user->save();
					}catch(Exception $ex){
						$request->session()->flash('status', 'danger');
						$request->session()->flash('message', sprintf('Can not add user data. Error : %s', $ex));
						return redirect()->route('register');
					}
					
				}catch(Exception $ex){
					$request->session()->flash('status', 'danger');
					$request->session()->flash('message', sprintf('Can not add employee data. Error : %s', $ex));
					return redirect()->route('register');
				}
				
				$request->session()->flash('status', 'success');
				$request->session()->flash('message', 'Registration Complete');
				return redirect()->route('register');
			}
		}
	}
}
