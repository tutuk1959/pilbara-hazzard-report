<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hazardreport extends Model
{
	protected $table = 'hazardreport';
	public $timestamps = false;
}
