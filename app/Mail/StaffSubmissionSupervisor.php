<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StaffSubmissionSupervisor extends Mailable
{
	use Queueable, SerializesModels;
	public $employee;
	public $hazardreport;
	public $supervisor;
	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct($employee, $hazardreport, $supervisor)
	{
		$this->employee = $employee;
		$this->hazardreport = $hazardreport;
		$this->supervisor = $supervisor;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build()
	{
		return $this->subject('You have report assigned to you')
		->view('mail.staff_submission_supervisor');
	}
}
