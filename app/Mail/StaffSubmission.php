<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class StaffSubmission extends Mailable
{
	use Queueable, SerializesModels;
	public $employee;
	public $hazardreport;

	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct($employee, $hazardreport)
	{
		$this->employee = $employee;
		$this->hazardreport = $hazardreport;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build()
	{
		return $this->subject('Hazard report submission')
		->view('mail.staff_submission_');
	}
}
