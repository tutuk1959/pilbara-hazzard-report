<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEmailReport extends Mailable
{
	use Queueable, SerializesModels;
	public $employee;
	public $hazardreport;
	public $supervisor;
	public $attachment;
	/**
	* Create a new message instance.
	*
	* @return void
	*/
	public function __construct($employee, $hazardreport, $supervisor, $attachment)
	{
		$this->employee = $employee;
		$this->hazardreport = $hazardreport;
		$this->supervisor = $supervisor;
		$this->attachment = $attachment;
	}

	/**
	* Build the message.
	*
	* @return $this
	*/
	public function build()
	{
		$url = $this->attachment;
		return $this->subject('Hazard Report Copy')
		->view('mail.report')
		->attach(public_path($url),[
			'mime' => 'application/pdf',
		]);
	}
}
