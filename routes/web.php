<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::group(['middleware' => ['auth', 'role:one|two|three']], function() {
	Route::get('/', 'user\dashboard@index')->name('home');
	Route::get('/home', 'user\dashboard@index')->name('dashboard');
	Route::get('/report_pdf/{idhazardreport}', 'submission\submission@exportpdf_stream')->name('pdfexport_stream');
	Route::get('/send_email/{idhazardreport}', 'submission\submission@send_email_report')->name('pdfexport_sendemail');
	Route::get('/report_details/{idhazardreport}', 'submission\submission@report_details')->name('report_details');
});
Route::group(['middleware' =>['auth', 'role:one|two']],function(){
	Route::get('/supervisor_report_list', 'submission\submission@supervisor_index')->name('supervisor_report_list');
	
	Route::get('/supervisor_submission/{idhazardreport}', 'submission\submission@supervisor')->name('supervisor_submission');
	Route::post('/supervisor_submission/', 'submission\submission@supervisorsubmission')->name('supervisor_submission_post');
});

Route::group(['middleware' =>['auth', 'role:one|three']],function(){
	Route::get('/staff_submission', 'submission\submission@index')->name('staff_submission');
	Route::post('/staff_submission', 'submission\submission@staffsubmission')->name('staff_submission_submit');
	Route::get('/mysubmission', 'submission\submission@mysubmission')->name('mysubmission');
	Route::get('/staff_report/{idhazardreport}', 'submission\submission@staffreport')->name('staff_report');
	Route::post('/staff_report', 'submission\submission@staffreportpost')->name('staff_report_update');
});

Route::get('/login', 'Auth\LoginController@index')->name('login');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/register', 'Auth\RegisterController@index')->name('register');
Route::post('/register', 'Auth\RegisterController@register')->name('register_submit');

/** AJAX STUFFS **/
Route::get('/load_department', 'submission\submission@loadDepartment')->name('load_department');
Route::get('/load_location', 'submission\submission@loadLocation')->name('load_location');
Route::get('/load_supervisor', 'submission\submission@loadSupervisor')->name('load_supervisor');
Route::get('/load_report', 'submission\submission@loadReport')->name('load_report');
